#!/bin/bash
# Filename: rpm-check.sh
# Invocation: rpm-check.sh some-file.rpm
# Use: Queries an rpm file for description, listing, and whether it can be installed.
# Saves output to a file.
# # # # #
# Return Codes
E_BADARGS=65
# # # # #
# Vars
INPUT_FILE="$1"
OUT_FORMAT="rpmtest"
OUTPUT_FILE=${1}.${OUT_FORMAT}
# # # # #
# Functions
usage() {
    echo "Usage: $0 <rpm-file>"
    echo "Queries an rpm file for description, listing, and whether it can be installed."
    echo "Saves output to a .${OUT_FORMAT} file."
    exit "${E_BADARGS}"
}
# # # # #
# "Main"
[[ $# -eq 1 ]] || usage
[[ -e "$1" ]] && {
    {
        echo "Archive Description:"
        echo -e "$(rpm -qpi ${INPUT_FILE})\n"
        echo "Archive Listing:"
        echo -e "$(rpm -qpl ${INPUT_FILE})\n"
        rpm -i --test "${INPUT_FILE}" 2&>1
    } > "${OUTPUT_FILE}" && echo "Results of rpm test in file ${OUTPUT_FILE}"
} || {
    echo "${1} does not appear to exist, exiting script now."
    exit "${E_BADARGS}"
}
exit $?